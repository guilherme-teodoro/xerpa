(defproject account-settings "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/clojurescript "1.7.228"]
                 [reagent "0.5.1"]
                 [reagent-forms "0.5.23"]
                 [reagent-reforms "0.4.3"]
                 [re-frame "0.7.0"]
                 [garden "1.3.2"]
                 [com.cemerick/valip "0.3.2"]]

  :min-lein-version "2.5.3"

  :source-paths ["src/clj"]

  :plugins [[lein-cljsbuild "1.1.3"]
            [lein-garden "0.2.6"]]

  :clean-targets ^{:protect false} ["resources/public/js/compiled" "target"
                                    "test/js"
                                    "resources/public/css"]

  :figwheel {:css-dirs ["resources/public/css"]}

  :garden {:builds [{:id           "screen"
                     :source-paths ["src/clj"]
                     :stylesheet   account-settings.css/screen
                     :compiler     {:output-to     "resources/public/css/screen.css"
                                    :pretty-print? true}}]}

    :profiles
  {:dev
   {:plugins [[lein-figwheel "0.5.2"]
              [lein-doo "0.1.6"]]
    :cljsbuild
    {:builds
     [{:id           "dev"
       :source-paths ["src/cljs"]
       :figwheel     {:on-jsload "account-settings.core/mount-root"}
       :compiler     {:main                 account-settings.core
                      :output-to            "resources/public/js/compiled/app.js"
                      :output-dir           "resources/public/js/compiled/out"
                      :asset-path           "js/compiled/out"
                      :source-map-timestamp true}}

      {:id           "test"
       :source-paths ["src/cljs" "test/cljs"]
       :compiler     {:output-to     "resources/public/js/compiled/test.js"
                      :main          account-settings.runner
                      :optimizations :none}}]}}

   :prod
   {:cljsbuild
    {:builds
     [{:id           "min"
       :source-paths ["src/cljs"]
       :compiler     {:main            account-settings.core
                      :output-to       "resources/public/js/compiled/app.js"
                      :optimizations   :advanced
                      :closure-defines {goog.DEBUG false}
                      :pretty-print    false}}]}}})

(ns account-settings.handlers
    (:require [re-frame.core :as re-frame]
              [account-settings.db :as db]
              [account-settings.utils :as u]))

(re-frame/register-handler
 :initialize-db
 (fn  [_ _]
   db/default-db))

(re-frame/register-handler
 :change-form
 (fn [db [_ val path]]
   (assoc-in db path val)))

(re-frame/register-handler
 :set-required
 (fn [db [_ id]]
   (assoc-in db [:account :errors id] "Is required")))

(re-frame/register-handler
 :set-error
 (fn [db [_ id]]
   (assoc-in db [:account :errors id] "Is invalid")))

(re-frame/register-handler
 :remove-error
 (fn [db [_ id]]
   (update-in db [:account :errors] dissoc id)))

(re-frame/register-handler
 :blur-form
 (fn [db [_ id]]
   (if (empty? (filter #(= % id) (-> db :account :pristines)))
     (update-in db [:account :pristines] conj id)
     db)))

(re-frame/register-handler
 :save
 (fn [db [_ data]]
   (u/set-item! "data" data)
   (.alert js/window "Stored in localStorage :)")
   db))


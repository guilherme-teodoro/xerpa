(ns account-settings.validation
  (:require [re-frame.core :refer [subscribe]]))

(def phone-pattern #"^(?:\(([0-9]{3})\)|([0-9]{3}))[-. ]?([0-9]{3})[-. ]?([0-9]{4})$")

(def email-pattern #"^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$")

(def url-pattern #"(([A-Za-z]{3,9})://)?([-;:&=\+\$,\w]+@{1})?(([-A-Za-z0-9]+\.)+[A-Za-z]{2,3})(:\d+)?((/[-\+~%/\.\w]+)?/?([&?][-\+=&;%@\.\w]+)?(#[\w]+)?)?")

(defn email [val]
  (not (empty? (re-find email-pattern val))))

(defn phone [val]
  (not (empty? (re-find phone-pattern val))))

(defn website [val]
  (not (empty? (re-find url-pattern val))))

(defn password [val]
  (> (count val) 8))

(defn confirm-password [val]
  (let [password (subscribe [:get-value :password])]
    (= val @password)))


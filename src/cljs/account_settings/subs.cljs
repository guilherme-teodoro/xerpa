(ns account-settings.subs
    (:require-macros [reagent.ratom :refer [reaction]])
    (:require [re-frame.core :as re-frame]))

(re-frame/register-sub
 :name
 (fn [db]
   (reaction (:name @db))))

(re-frame/register-sub
 :form-account
 (fn [db]
   (reaction (:account @db))))

(re-frame/register-sub
 :has-err?
 (fn [db [_ id]]
   (reaction (not (empty? (-> @db :account :errors))))))

(re-frame/register-sub
 :get-value
 (fn [db [_ id]]
   (reaction (-> @db :account id))))

(re-frame/register-sub
 :error
 (fn [db [_ id]]
   (reaction (-> @db :account :errors id))))

(re-frame/register-sub
 :pristine?
 (fn [db [_ id]]
   (reaction (> (.indexOf (to-array (-> @db :account :pristines)) id) -1))))

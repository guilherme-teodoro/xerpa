(ns account-settings.components.account-create
  (:require [account-settings.components.input :as i]
            [account-settings.validation :as v]
            [re-frame.core :refer [dispatch]]))

(defn handle-click [form]
  (dispatch [:save form]))

(defn form [form has-err?]

  [:div
   [i/input "name" {:id :name
                    :required? true
                    :type "text"
                    :validation-params (:name form)
                    :validation (fn [] true)}]

   [i/input "email" {:id :email
                     :required? true
                     :type "text"
                     :validation-params (:email form)
                     :validation v/email}]

   [i/input "phone" {:id :phone
                     :required? true
                     :type "text"
                     :validation-params (:phone form)
                     :validation v/phone}]

   [i/input "website" {:id :website
                       :required? true
                       :type "text"
                       :validation-params (:website form)
                       :validation v/website}]

   [i/input "password" {:id :password
                        :required? true
                        :type "password"
                        :validation-params (:password form)
                        :validation v/password}]

   [i/input "confirm password" {:id :confirm-password
                                :type "password"
                                :required? true
                                :validation v/confirm-password
                                :validation-params (:confirm-password form)}]
   [:div.panel-form__footer
    [:button.panel-btn {:type "button" :disabled has-err? :on-click #(handle-click form)}
     "Salvar"]]])

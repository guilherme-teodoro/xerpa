(ns account-settings.components.sidebar)

(defn avatar []
  [:div.sidebar-avatar
   [:img.sidebar-avatar__image {:src "images/avatar.png"}]
   [:div.sidebar-avatar__name "Lauren Clifford"]
   [:div.sidebar-avatar__description "I enjoy photography and I have that little bit of quirk you'll find interesting!"]])

(defn sidebar-menu []
  [:div.sidebar-menu
   [:div.sidebar-menu__header "Account settings"]
   [:ul.sidebar-menu__list
    [:li.sidebar-menu__item
     [:span "Profile"]
     [:img {:src "images/profile_icon.png"}]]
    [:li.sidebar-menu__item
     [:span "Vacancies"]
     [:img {:src "images/vacancies_icon.png"}]]]])

(defn sidebar []
  [:sidebar.sidebar
   [avatar]
   [sidebar-menu]])



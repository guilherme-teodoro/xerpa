(ns account-settings.components.input
  (:require [re-frame.core :refer [dispatch subscribe]]
            [clojure.string :refer [replace]]
            [reagent.core :as r]))

(defn valid? [validation args]
  (apply validation [args]))

(defn error-message [id]
  (let [error (subscribe [:error id])
        pristine? (subscribe [:pristine? id])]
    (when (and @error @pristine?) [:span.form-control__error "!"
           [:span.form-control__error-msg @error]])))

(defn success-message [id]
  (let [error (subscribe [:error id])
        pristine? (subscribe [:pristine? id])]
    (when (and (not @error) @pristine?)
      [:span.form-control__success [:img {:src "images/Confirm_icon.png"}]])))

(defn input [label {:keys [type id required? validation validation-params error]} path]
  (r/create-class
   {:component-did-mount
    (fn []
      (if (true? required?) (dispatch [:set-required id])))
    :reagent-render
    (fn []
      [:div.form-control
       [:label.form-control__label {:for (replace (str id) ":" "")} label]
       [:input.form-control__input
        {:id id
         :type type
         :on-blur (fn [e]
                    (cond
                      (and (empty? (-> e .-target .-value)) (true? required?))
                      (dispatch [:set-required id])

                      (not (valid? validation (-> e .-target .-value)))
                      (dispatch [:set-error id])

                      :else
                      (dispatch [:remove-error id]))
                    (dispatch [:blur-form id]))
         :on-change #(dispatch [:change-form (-> % .-target .-value) [:account id]])}]
       [error-message id]
       [success-message id]])}))

(ns account-settings.views
  (:require [re-frame.core :as re-frame :refer [dispatch subscribe]]
            [reagent.core :as r]
            [reagent-forms.core :refer [bind-fields]]
            [clojure.string :refer [replace]]
            [account-settings.components.sidebar :as s]
            [account-settings.components.account-create :as c]))

(defn form []
  (let [has-err? (subscribe [:has-err?])
        f (subscribe [:form-account])]
    (fn []
      [:div.panel-form
       [:h1.panel-form__header "Account Settings"]
       [:form
        [c/form @f @has-err?]]])))

(defn main-panel []
  (let [name (re-frame/subscribe [:name])]
    (fn []
      [:div.panel
       [s/sidebar]
       [form]])))


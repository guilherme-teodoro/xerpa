(ns account-settings.css
  (:require [garden.def :refer [defstyles]]
            [garden.stylesheet :refer [at-media]]))

(defstyles screen
  [:* {:box-sizing "border-box"}]
  [:body {:background-image "url(/images/background.jpg)"
          :margin 0
          :font-family "'Open Sans', Arial"
          :background-size "cover"
          :background-attachment "fixed"
          :background-repeat "no-repeat"}]

  [:#app {:display "flex"
          :position "absolute"
          :left 0
          :top 0
          :bottom 0
          :align-items "center"
          :justify-content "center"
          :width "100%"
          :height "100%"}]

  [:.panel {:max-width "100%"
            :width "545px"
            :border-radius "4px"
            :overflow "hidden"
            :display "flex"
            :flex-direction "row"
            :box-shadow "0 0 15px rgba(41, 53, 57, 0.32)"}]

  [:.sidebar {:display "flex"
              :width "170px"
              :flex-direction "column"}]

  [:.sidebar-avatar {:background-color "rgba(0, 175, 240, 0.60)"
                     :text-align "center"
                     :padding "30px 15px 15px 20px"
                     :color "white"}]

  [:.sidebar-avatar__name {:font-size "13px"
                           :margin "8px 0"}]

  [:.sidebar-avatar__description {:font-size "7px"
                                  :color "rgba(255, 255, 255, 0.50)"}]

  [:.sidebar-avatar__image {:border-radius "50%"
                            :border "2px solid white"}]

  [:.sidebar-menu {:background-color "#00AFF0"
                   :Flex 1}]

  [:.sidebar-menu__header {:font-size "9px"
                           :text-transform "uppercase"
                           :color "rgba(255, 255, 255, 0.60)"
                           :padding-top "30px"
                           :padding-left "25px"
                           :padding-right "25px"
                           :padding-bottom "20px"}]

  [:.sidebar-menu__list {:list-style "none"
                         :padding "0"
                         :margin "0"}]

  [:.sidebar-menu__item {:color "white"
                         :font-size "12px"
                         :display "flex"
                         :transition "background 200ms ease-in-out"
                         :align-items "center"
                         :padding "10px 25px"
                         :cursor "pointer"}

   [:&:hover {:background-color "#00a4e0"}]

   [:span {:flex "1"}]]

  [:.panel-form {:padding "30px"
              :flex "1"
              :background "white"}]

  [:.panel-form__header {:font-size "24px"
                         :color "#3C494E"
                         :margin-top "0"
                         :margin-bottom "25px"
                         :font-weight 300}]

  [:.panel-form__footer {:display "flex"
                         :justify-content "flex-end"}]

  [:.panel-btn {:background "#00AFF0"
                :font-size "14px"
                :color "white"
                :border "none"
                :border-radius "2px"
                :padding "10px 40px"}

   [:&:disabled {:background "#ccc"}]]

  [:.form-control {:width "100%"
                   :background "white"
                   :border "1px solid #E2E4E5"
                   :margin-bottom "10px"
                   :position "relative"}]

  [:.form-control__label {:text-transform "uppercase"
                          :display "block"
                          :color "#c9c9c9"
                          :cursor "text"
                          :padding "5px 10px 0"
                          :font-size "10px"}]

  [:.form-control__input {:border 0
                          :width "100%"
                          :padding "0 10px 5px"
                          :font-size "14px"
                          :color "#343E41"
                          :outline "none"}]

  [:.form-control__error {:position "absolute"
                          :right "7px"
                          :width "30px"
                          :height "30px"
                          :display "block"
                          :top "7px"
                          :border "2px solid #f24f32"
                          :border-radius "50%"
                          :text-weight "bold"
                          :color "#f24f32"
                          :line-height "25px"
                          :text-align "center"
                          :font-size "24px"}
   [:&:hover [:.form-control__error-msg {:opacity 1}]]]

  [:.form-control__error-msg {:opacity 0
                              :position "absolute"
                              :right "40px"
                              :top "0px"
                              :font-size "12px"
                              :line-height "13px"
                              :text-align "right"
                              :color "#f24f32"}]

  [:.form-control__success {:position "absolute"
                            :right "10px"
                            :top "15px"}]

  (at-media
   {:max-width "545px"}

   [:body {:background-attachment "inherit"}]

   [:#app {:display "block"}]

   [:.panel {:flex-direction "column"
             :display "block"}]

   [:.sidebar {:width "100%"}]

   [:.sidebar-menu {:padding-bottom "20px"}]

   [:.panel-btn {:width "100%"
                 :display  "block"}]))

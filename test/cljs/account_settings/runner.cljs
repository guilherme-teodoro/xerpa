(ns account-settings.runner
    (:require [doo.runner :refer-macros [doo-tests]]
              [account-settings.core-test]))

(doo-tests 'account-settings.core-test)
